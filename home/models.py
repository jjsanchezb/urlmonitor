from django.db import models
from datetime import datetime
from pathlib import Path
from django.db.models.signals import post_delete
from django.dispatch import receiver
import multiprocessing
import os


class ListUrlsModel(models.Model):
    """File with urls"""
    name = models.TextField()
    file = models.FileField(upload_to='txt/')

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        self.name = self.file.name
        super().save(*args, **kwargs)


@receiver(post_delete, sender=ListUrlsModel)
def list_urls_delete_file(sender, instance, **kwargs):
    instance.file.delete(False)


class UrlsModel(models.Model):
    """urls"""
    url = models.TextField(unique=True)


class UrlTimeline(models.Model):
    """Timeline to one URL"""

    # folder is equal to url
    folder = models.TextField()


class UrlSnapshot(models.Model):
    """One Snapshot to one URL"""

    # folder is equal to time of snapshot
    folder = models.TextField()
    url_timeline = models.ForeignKey('UrlTimeline', on_delete=models.CASCADE)

    def save(self, *args, **kwargs):
        self.folder = datetime.now().strftime("%Y-%m-%d-%H-%M-%S")
        super().save(*args, **kwargs)


def url_snapshot_get_folder_path(instance, filename):
    joined_path = '/'.join([instance.url_snapshot.url_timeline.folder.rstrip(),
                            instance.url_snapshot.folder, filename])
    full_path = os.fspath(Path(joined_path))
    # https://docs.microsoft.com/es-es/windows/win32/fileio/naming-a-file
    # https://www.w3schools.com/tags/ref_urlencode.asp
    full_path = full_path.replace(":", "%3A")
    full_path = full_path.replace("?", "%3F")

    return full_path


class UrlSnapshotFile(models.Model):
    """File of Url Snapshot."""
    url_snapshot = models.ForeignKey('UrlSnapshot', on_delete=models.CASCADE)
    file = models.FileField(
        upload_to=url_snapshot_get_folder_path, max_length=500)


@receiver(post_delete, sender=UrlSnapshotFile)
def url_snapshot_delete_file(sender, instance, **kwargs):
    instance.file.delete(False)


class InternalSettings(models.Model):
    time_between_monitoring = models.IntegerField(default=60)
    time_js_await = models.IntegerField(default=5)
    urls_simultaneously = models.IntegerField(default=5)
    num_browsers = models.IntegerField(
        default=multiprocessing.cpu_count() // 2)
    date_added = models.DateTimeField(auto_now=True)

    class Meta:
        get_latest_by = 'date_added'


class UrlSnapshotDifferent(models.Model):
    file_path = models.TextField()
    date_added = models.DateTimeField(auto_now=True)
