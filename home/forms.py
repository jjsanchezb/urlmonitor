from django.forms import ModelForm
from .models import ListUrlsModel, UrlTimeline, InternalSettings, UrlSnapshotDifferent
from django.forms import Textarea


class ListUrlsForm(ModelForm):

    class Meta:
        model = ListUrlsModel
        fields = ['file']


class UrlTimelineForm(ModelForm):

    class Meta:
        model = UrlTimeline
        fields = ['folder']
        widgets = {
            'folder': Textarea(attrs={'class': 'materialize-textarea'}),
        }


class InternalSettingsForm(ModelForm):

    class Meta:
        model = InternalSettings
        exclude = ['data_added']


class UrlSnapshotDifferentForm(ModelForm):

    class Meta:
        model = UrlSnapshotDifferent
        fields = '__all__'
        widgets = {
            'file_path': Textarea(attrs={'class': 'materialize-textarea', 'readonly': 'True'}),
        }
