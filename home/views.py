from django.shortcuts import render
from django.http import JsonResponse
from django.forms.models import model_to_dict
from multiprocessing import Process, Array
from home.utils.ListUrlManager import Monitor
from django.db import connections
from pathlib import Path
from .forms import ListUrlsForm, InternalSettingsForm, UrlSnapshotDifferentForm
from .models import ListUrlsModel, UrlsModel, InternalSettings, UrlSnapshotDifferent, UrlTimeline
import math
import time
import logging
import shutil

format_loggin = 'Process name:%(processName)s time:%(asctime)s level:%(levelname)s message:%(message)s'.format(
)
logging.basicConfig(filename='logg.log',
                    format=format_loggin, level=logging.INFO)
monitors = {}

processes = {}


def index(request):

    if request.method == 'GET':

        request.session['files'] = list(
            ListUrlsModel.objects.values_list('name'))
        request.session['urls'] = list(UrlsModel.objects.values_list('url'))
        last_settings = model_to_dict(InternalSettings.objects.latest())
        num_notifications = UrlSnapshotDifferent.objects.all().count()
        context = {
            'form_file_with_urls': ListUrlsForm(),
            'form_internal_settings': InternalSettingsForm(initial=last_settings),
            'num_notifications': num_notifications}
        return render(request, 'home/index.html', context)

    if request.method == 'POST':

        form_file_with_urls = ListUrlsForm(request.POST, request.FILES)
        internal_settings = InternalSettingsForm(
            request.POST, request.FILES)
        if form_file_with_urls.is_valid():

            list_urls = []
            model_file_with_urls = form_file_with_urls.save()
            # To keep file names after refresh page or come back to page
            request.session[
                'files'] = list(ListUrlsModel.objects.values_list('name'))
            text_file_with_urls = model_file_with_urls.file.read()
            # decode  is necessary to remove  instance of the bytes and get str
            # instance
            list_urls = text_file_with_urls.decode('utf-8').split("\n")
            # To keep list of urls after refresh page or come back to page
            for url in list_urls:
                try:
                    if url != "":
                        UrlsModel(url=url).save()
                except Exception as e:
                    logging.warning("url failed: " + str(url) +
                                    " Error: " + str(e))
            # To keep list of urls after refresh page or come back to page
            request.session[
                'urls'] = list(UrlsModel.objects.all().values_list('url'))

            last_settings = model_to_dict(InternalSettings.objects.latest())
            num_notifications = UrlSnapshotDifferent.objects.all().count()

            context = {
                'form_file_with_urls': ListUrlsForm(),
                'form_internal_settings': InternalSettingsForm(initial=last_settings),
                'num_notifications': num_notifications
            }
            return render(request, 'home/index.html', context)
        if internal_settings.is_valid():
            last_settings = internal_settings.save()
            last_settings_dict = model_to_dict(
                last_settings)
            request.session['files'] = list(
                ListUrlsModel.objects.values_list('name'))
            request.session['urls'] = list(
                UrlsModel.objects.values_list('url'))

            if len(monitors) > 0:
                for key, value in monitors.items():
                    value['shared'][2] = last_settings.time_between_monitoring
                    value['shared'][3] = last_settings.time_js_await
                    value['shared'][4] = last_settings.urls_simultaneously

            num_notifications = UrlSnapshotDifferent.objects.all().count()
            context = {
                'form_file_with_urls': ListUrlsForm(),
                'form_internal_settings': InternalSettingsForm(initial=last_settings_dict),
                'num_notifications': num_notifications
            }
            return render(request, 'home/index.html', context)


def notifications(request):

    if request.method == 'POST':

        form_file_with_url = UrlSnapshotDifferentForm(
            request.POST)
        if form_file_with_url.is_valid():
            url_to_delete = form_file_with_url.cleaned_data['file_path']
            query_url_to_delete = UrlSnapshotDifferent.objects.filter(
                file_path=url_to_delete[14:])
            if query_url_to_delete.count() == 1:
                query_url_to_delete.delete()

    files = UrlSnapshotDifferent.objects.all()
    forms = [UrlSnapshotDifferentForm(
        initial={'file_path': "localhost:8000" + file.file_path}) for file in files]
    context = {
        'notification_forms': forms
    }

    return render(request, 'home/notification.html', context)


def start_monitors(all_urls):

    internal_setting = InternalSettings.objects.latest()
    num_urls = len(all_urls)
    num_urls_by_monitor = math.ceil(num_urls / internal_setting.num_browsers)
    num_monitor = 0
    for i in range(0, num_urls, num_urls_by_monitor):

        urls_to_monitor = all_urls[i:i + num_urls_by_monitor]
        # index 0:Activate browsers index 1:Stop browsers
        shared = Array('i', [0, 0, internal_setting.time_between_monitoring,
                             internal_setting.time_js_await, internal_setting.urls_simultaneously])
        monitors[num_monitor] = {'monitor': Monitor(), 'shared': shared}
        # Close database connections to let other process create your
        # connexions
        connections.close_all()
        p = Process(target=monitors[num_monitor]['monitor'].monitor_urls, args=(
            urls_to_monitor, monitors[num_monitor]['shared']))
        p.start()
        num_monitor += 1


def start_monitor(request):
    if request.method == 'GET':

        if len(monitors) == 0:

            list_urls = list(UrlsModel.objects.values_list('url', flat=True))
            internal_setting = InternalSettings.objects.latest()
            start_monitors(list_urls)
            monitors_started = 0

            while (monitors_started < internal_setting.num_browsers):
                time.sleep(1)
                monitors_started = 0
                for key, value in monitors.items():
                    if value['shared'][0] == 1:
                        monitors_started += 1

            request.session['monitor_started'] = True
            return JsonResponse({'monitor_started': True})
        else:
            return JsonResponse({'monitor_started': False})


def stop_monitor(request):

    if request.method == 'GET':

        if len(monitors) != 0:

            for key, value in monitors.items():
                value['shared'][1] = 1

            monitors_stoped = 0
            internal_setting = InternalSettings.objects.latest()
            while (monitors_stoped < internal_setting.num_browsers):

                time.sleep(1)
                monitors_stoped = 0
                for key, value in monitors.items():
                    if value['shared'][1] == 0:
                        monitors_stoped += 1

            request.session['monitor_started'] = False
            monitors.clear()
            print('monitors', monitors)
            return JsonResponse({'monitor_stoped': True})
        else:
            return JsonResponse({'monitor_stoped': False})


def clean_urls_notifications(request):

    if request.method == 'GET':

        try:

            ListUrlsModel.objects.all().delete()
            logging.info('files with urls deleted')
            urls = UrlsModel.objects.all()
            for url in urls:
                url_timeline = UrlTimeline.objects.filter(folder=url.url)
                url_timeline.delete()
            urls = UrlsModel.objects.all().delete()
            logging.info('urls deleted')
            UrlSnapshotDifferent.objects.all().delete()
            logging.info('urls files deleted')
            if Path("media").exists():
                shutil.rmtree("media")
            logging.info('media folder deleted')
            return JsonResponse({'cleaned': True})
        except Exception as e:
            logging.error('error while deleting: ' +
                          ' Error: ' + str(e))
            return JsonResponse({'cleaned': False})
