function deleteScripts_and_NoScripts() {
    let scripts = document.getElementsByTagName('script');
    for (let i = scripts.length - 1; i >= 0; i--) {
        const element = scripts[i];
        element.parentNode.removeChild(element);
    }
    let noscripts = document.getElementsByTagName('noscript');
    for (let i = noscripts.length - 1; i >= 0; i--) {
        const element = noscripts[i];
        element.parentNode.removeChild(element);
    }
}