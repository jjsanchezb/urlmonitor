function cleanAllComments() {
    let comments = [];
    let iterator = document.createNodeIterator(document, NodeFilter.SHOW_COMMENT, function() {
        return NodeFilter.FILTER_ACCEPT;
    });
    let curNode;
    while (curNode = iterator.nextNode()) {
        comments.push(curNode);
    }

    for (let i = comments.length - 1; i >= 0; i--) {
        const element = comments[i];
        element.parentNode.removeChild(element);
    }
}