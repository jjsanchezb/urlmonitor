function deleteCss() {
    let css_ = document.getElementsByTagName('style')
    for (let i = css_.length - 1; i >= 0; i--) {
        const element = css_[i];
        element.parentNode.removeChild(element);
    }
}