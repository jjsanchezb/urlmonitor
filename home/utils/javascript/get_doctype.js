function get_document_and_html_tag() {
    let node = document.doctype;
    let document_tag = "<!DOCTYPE " +
        node.name +
        (node.publicId ? ' PUBLIC "' + node.publicId + '"' : '') +
        (!node.publicId && node.systemId ? ' SYSTEM' : '') +
        (node.systemId ? ' "' + node.systemId + '"' : '') +
        '>';

    let html_tag = document.documentElement.outerHTML.match(/(<htm)[^>]+>/g);
    let result = [document_tag, html_tag[0]];
    return result;
}