import asyncio
from aiofile import AIOFile
from pyppeteer import launch
import os


class PyppeteerManager:

    def __init__(self):
        self.browser = None

    async def setup(self):
        """Initialize chrome instance with web driver"""

        self.browser = await launch(headless=False, handleSIGINT=False,
                                    handleSIGTERM=False,
                                    handleSIGHUP=False)

    async def add_page(self, url):
        """Add page in Chromium browser"""
        page = await self.browser.newPage()
        await page.goto(url, timeout=30000)
        return page

    async def clean_scripts_and_no_scripts(self, page):
        """Do page static, removing script and noscript tags"""
        path_javascript = os.path.join(os.path.dirname(
            __file__), 'javascript', 'do_web_page_static.js')

        async with AIOFile(path_javascript, mode='r') as javascript:
            javascript_text = await javascript.read()
            await page.evaluate(javascript_text)

    async def clean_comments(self, page):
        """Clean the html of page from comments"""
        path_javascript = os.path.join(os.path.dirname(
            __file__), 'javascript', 'clean_web_page_from_comments.js')
        async with AIOFile(path_javascript, mode='r') as javascript:
            javascript_text = await javascript.read()
            await page.evaluate(javascript_text)

    async def clean_css(self, page):
        """Clean the html of page from style tags"""
        path_javascript = os.path.join(os.path.dirname(
            __file__), 'javascript', 'clean_web_page_from_style_tags.js')
        async with AIOFile(path_javascript, mode='r') as javascript:
            javascript_text = await javascript.read()
            await page.evaluate(javascript_text)

    async def get_html(self, page):
        html = await page.content()
        return html

    async def get_css(self, page):
        css = await page.evaluate("""()=> {return getComputedStyle(document.documentElement)}""")
        return css

    async def get_body(self, page):
        body = await page.evaluate("""()=> {return document.body.innerHTML}""")
        return body

    async def get_head(self, page):
        head = await page.evaluate("""()=> {return document.head.outerHTML}""")
        return head

    async def close_page(self, page):
        await page.close()

    async def close_browser(self):
        await self.browser.close()

    async def get_document_JSHandle(self, page):
        return await page.evaluateHandle('document')

    async def get_document_and_html_tags(self, page):
        path_javascript = os.path.join(os.path.dirname(
            __file__), 'javascript', 'get_doctype.js')
        async with AIOFile(path_javascript, mode='r') as javascript:
            javascript_text = await javascript.read()
            document_html = await page.evaluate(javascript_text)
            return document_html

    async def get_url(self, page):
        url = await page.evaluate("""()=> {return window.location.protocol + "//" + window.location.host+  window.location.pathname}""")
        return url

    async def get_pages(self):
        pages = await self.browser.pages()
        return pages
