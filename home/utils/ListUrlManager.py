from comparePageTimeline.wsgi import application
from home.utils.PyppeteerManager import PyppeteerManager
from home.models import UrlSnapshot, UrlSnapshotFile, UrlTimeline, UrlSnapshotDifferent
from django.core.files.base import ContentFile
from home.utils.htmldiff import render_html_diff
import time
import asyncio
import logging
import regex
import os


class Monitor:

    def __init__(self):

        self.browser_manager = None
        # Avoid send instructions to broser closed
        self.browser_manager_running = False
        self.stop_monitoring = False
        self.loop = None
        self.stacks_urls = {}
        self.number_urls = 0
        self.number_urls_sleeping = 0
        self.regex_src = regex.compile(r'src="(?!http|\/)')
        self.regex_href = regex.compile(r'href="(?!http|tel|\/)')
        # manage of references that start with /
        self.regex_src_base = regex.compile(r'src="(?!http)\/+')
        self.regex_href_base = regex.compile(r'href="(?!http|tel)\/+')
        self.regex_empy_diff_tags = regex.compile(
            r'(<del><\/del>|<ins><\/ins>)')
        self.num_actual_urls = 0

    def setup(self):

        format_loggin = 'Process name:%(processName)s time:%(asctime)s level:%(levelname)s message:%(message)s'.format(
        )
        logging.basicConfig(filename='logg.log',
                            format=format_loggin, level=logging.INFO)
        self.loop = asyncio.new_event_loop()
        asyncio.set_event_loop(self.loop)
        self.browser_manager = PyppeteerManager()
        self.loop.run_until_complete(self.browser_manager.setup())

    async def url_monitor(self, url, model_url_timeline, shared):

        try:

            # Get static and clean html
            logging.info('Added to browser:  ' + url)
            page = await self.browser_manager.add_page(url)

            await asyncio.sleep(shared[3])
            await self.browser_manager.clean_scripts_and_no_scripts(page)
            await self.browser_manager.clean_comments(page)
            html = await self.browser_manager.get_html(page)
            # Correct src and href urls from relative to absolute
            base_url = await self.browser_manager.get_url(page)
            base_url = '/'.join(base_url.split('/')[:-1])
            base_url += '/'
            logging.info('Before regex: ' + url)
            html = self.regex_src.sub('src="' + base_url, html)
            html = self.regex_href.sub('href="' + base_url, html)
            url_base = '/'.join(base_url.split('/')[0:3])
            url_base += '/'
            html = self.regex_src_base.sub('src="' + url_base, html)
            html = self.regex_href_base.sub('href="' + url_base, html)
            # Save html snapshot
            content_file = ContentFile(html.encode('utf-8'))
            logging.info('Before do models: ' + url)
            model_url_snapshot = UrlSnapshot(
                url_timeline=model_url_timeline)
            model_url_snapshot.save()

            model_url_snapshot_file = UrlSnapshotFile(
                url_snapshot=model_url_snapshot)
            model_url_snapshot_file.file.save('main.html', content_file)
            model_url_snapshot_file.save()
            logging.info('Before get head and body: ' + url)
            # Only body can be compared
            body = await self.browser_manager.get_body(page)
            # html reconstruction required after comparison of bodies
            head = await self.browser_manager.get_head(page)
            self.stacks_urls[url].append(body)
            if len(self.stacks_urls[url]) > 1:
                # Get original tags doctype and html  from beginning of
                # document
                logging.info('Searching differences: ' + url)
                initial_tags = await self.browser_manager.get_document_and_html_tags(
                    page)
                # Build html after comparison
                html_diff = ' '.join(
                    [initial_tags[0], initial_tags[1], head, '<body>'])
                html_diff += render_html_diff(
                    self.stacks_urls[url][-2], self.stacks_urls[url][-1])
                html_diff += ''.join(
                    ['<style> del {background: orange;}ins {background: green;} </style>', "</body>", "</html>"])

                # Correct src and href urls from relative to absolute
                html_diff = self.regex_src.sub(
                    'src="' + base_url, html_diff)
                html_diff = self.regex_href.sub(
                    'href="' + base_url, html_diff)
                html_diff = self.regex_src_base.sub(
                    'src="' + url_base, html_diff)
                html_diff = self.regex_href_base.sub(
                    'href="' + url_base, html_diff)

                # Correct empy tags ins or del
                html_diff = self.regex_empy_diff_tags.sub('', html_diff)

                if '<ins' in html_diff or '<del' in html_diff:
                    # Save html with differences
                    logging.info('Differences founded: ' + url)
                    content_file = ContentFile(html_diff.encode('utf-8'))
                    model_url_snapshot_file = UrlSnapshotFile(
                        url_snapshot=model_url_snapshot)
                    model_url_snapshot_file.file.save(
                        'diff.html', content_file)
                    model_url_snapshot_file.save()
                    full_url_file = '{0}'.format(
                        model_url_snapshot_file.file.url)
                    UrlSnapshotDifferent(
                        file_path=full_url_file).save()
                self.stacks_urls[url].pop(0)

            await self.browser_manager.close_page(page)
            self.number_urls_sleeping += 1
            logging.info('Verifing stop signal: ' + url)
            if shared[1] == 1 and self.number_urls_sleeping == self.num_actual_urls:
                await self.browser_manager.close_browser()
                self.browser_manager_running = False
                shared[0] = 0
                shared[1] = 0
                logging.info('browser stoped')

        except Exception as e:
            self.number_urls_sleeping += 1
            logging.error('Error while processing url: ' +
                          url + ' Error' + str(e))

            if self.number_urls_sleeping == self.num_actual_urls:
                pages = await self.browser_manager.get_pages()
                for i in range(1, len(pages)):
                    page = pages[i]
                    await self.browser_manager.close_page(page)

            if shared[1] == 1 and self.number_urls_sleeping == self.num_actual_urls:
                logging.info("Verifing stop signal: " + url)
                await self.browser_manager.close_browser()
                self.browser_manager_running = False
                shared[0] = 0
                shared[1] = 0
                logging.info('browser stoped')

    def monitor_urls(self, urls, shared):

        if len(urls) > 0:

            self.setup()
            # Informate that browser start
            shared[0] = 1
            self.browser_manager_running = True
            self.number_urls = len(urls)

            # Keep multiples body of each url
            for k in range(0, self.number_urls):
                temp_url = urls[k]
                self.stacks_urls[temp_url] = []

            while self.browser_manager_running is True:
                start_time = time.time()
                # Divide urls in groups
                for k in range(0, self.number_urls, shared[4]):

                    tasks = []

                    if k + shared[4] > self.number_urls:
                        urls_simultaneusly = urls[k:]
                    else:
                        urls_simultaneusly = urls[k:k + shared[4]]

                    self.num_actual_urls = len(urls_simultaneusly)
                    self.number_urls_sleeping = 0
                    # Concurrency to urls simultaneusly
                    for i in range(len(urls_simultaneusly)):

                        url = urls_simultaneusly[i]

                        query_url = UrlTimeline.objects.filter(folder=url)
                        if query_url.count() == 1:
                            model_url_timeline = query_url.get()
                        else:
                            model_url_timeline = UrlTimeline(folder=url)
                            model_url_timeline.save()

                        tasks.append(self.loop.create_task(
                            self.url_monitor(url, model_url_timeline, shared)))

                    self.loop.run_until_complete(asyncio.wait(tasks))

                    if self.browser_manager_running is False:
                        break
                # Let browser stop while awaiting
                while True:
                    logging.info("Verifing stop signal: ")
                    if shared[1] == 1:
                        self.loop.run_until_complete(
                            self.browser_manager.close_browser())
                        self.browser_manager_running = False
                        shared[0] = 0
                        shared[1] = 0
                        logging.info('browser stoped')
                    time.sleep(2)
                    time_to_sleep = shared[2] - (time.time() - start_time)
                    if time_to_sleep < 0 or self.browser_manager_running is False:
                        break

            self.loop.close()
