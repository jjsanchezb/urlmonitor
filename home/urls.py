from django.urls import path

from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('start_monitor', views.start_monitor, name='start_monitor'),
    path('stop_monitor', views.stop_monitor, name='stop_monitor'),
    path('notifications', views.notifications, name='notifications'),
    path('clean_urls_notifications', views.clean_urls_notifications,
         name='clean_urls_notifications'),
]
